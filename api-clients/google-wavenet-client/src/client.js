const fs = require('fs');

const ApiClient = {

    callGoogleApiAndWriteToFile: (client, request, dir) => {

        client.synthesizeSpeech(request, (err, response) => {
            if (err) {
                console.error('ERROR:', err);
                return;
            }

            const file = ApiClient.getFilename(request, dir);

            // Write the binary audio content to a local file
            fs.writeFile(file, response.audioContent, 'binary', err => {
                if (err) {
                    console.error('ERROR:', err);
                    return;
                }
                console.log(file);
            });

            // Append the result to the index file
            const indexRow = ApiClient.requestToString(request, file);
            const indexFile = dir + '/robots_google.csv';
            fs.appendFile(indexFile, indexRow, err => {
                if (err) {
                    console.error('ERROR:', err);
                }
            });
        });
    },

    getFilename: (request, dir) => {
        const timestamp = Date.now();
        return dir + '/all/robot_' + request.voice.ssmlGender.toLowerCase() + '_' + request.voice.name + '_' + timestamp + '.wav';
    },

    requestToString: (request, filename) => {
        return "\"" + filename + "\", \""
            + request.voice.name + "\", \""
            + request.voice.ssmlGender + "\", \""
            + request.audioConfig.speakingRate + "\", \""
            + request.audioConfig.pitch + "\", \""
            + request.audioConfig.volumeGainDb + "\", \""
            + request.input.text.replace("\"", "\"\"") + "\",\n";
    }

};

module.exports = ApiClient;