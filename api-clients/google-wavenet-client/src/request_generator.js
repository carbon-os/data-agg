const Voices = require('./voices');
const Sentences = require('./sentences');

const RequestGenerator = {

    AUDIO_ENCODING: 'LINEAR16',

    SAMPLE_RATE: 16000,

    generateRequest: () => {
        return {
            input: {
                text: RequestGenerator.generateRandomText(),
            },
            voice: RequestGenerator.generateRandomVoice(),
            audioConfig: {
                speakingRate: RequestGenerator.generateRandomSpeakingRate(),
                pitch: RequestGenerator.generateRandomPitch(),
                volumeGainDb: RequestGenerator.generateRandomVolume(),
                audioEncoding: RequestGenerator.AUDIO_ENCODING,
                sampleRateHertz: RequestGenerator.SAMPLE_RATE,
            },
        };
    },

    defaultRequest: () => {
        return {
            input: {
                text: "Meghan, my darling, I'll love you forever."
            },
            voice: {
                name: 'en-US-Wavenet-D',
                languageCode: 'en-US',
                ssmlGender: 'MALE'
            },
            audioConfig: {
                speakingRate: 0,
                pitch: 1.00,
                volumeGainDb: 0,
                audioEncoding: 'LINEAR16',
                sampleRateHertz: 16000,
            },
        };
    },

    generateRandomSpeakingRate: () => {
//        return RequestGenerator.randomIntNormalDistribution(0.81, 1.23, 1)
        return RequestGenerator.randomIntNormalDistribution(0.60, 1.40, 1)
    },

    generateRandomPitch: () => {
//        return RequestGenerator.randomIntNormalDistribution(-5.20, 5.20, 1);
        return RequestGenerator.randomIntNormalDistribution(-10.00, 10.00, 1);
    },

    generateRandomVolume: () => {
//        return RequestGenerator.randomIntNormalDistribution(-5.20, 5.20, 1);
        return RequestGenerator.randomIntNormalDistribution(-10, 14, 1);
    },

    generateRandomVoice: () => {
        const index = RequestGenerator.randomInt(0, Voices.length);

        const voice = Voices[index];
        const languageCodeIndex = RequestGenerator.randomInt(0, voice.languageCodes.length);
        const languageCode = voice.languageCodes[languageCodeIndex];

        return {
            name: voice.name,
            ssmlGender: voice.ssmlGender,
            languageCode: languageCode,
        }
    },

    generateRandomText: () => {
        const index = RequestGenerator.randomInt(0, Sentences.length);
        return Sentences[index];
    },

    randomInt: (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; // The maximum is exclusive and the minimum is inclusive
    },

    // it will make it to ~half of the min max some low percentage of the time ~2%
    // it will pick the mean ~40% of the time
    randomIntNormalDistribution: (min, max, skew) => {
        let u = 0, v = 0;
        while (u === 0) u = Math.random(); //Converting [0,1) to (0,1)
        while (v === 0) v = Math.random();
        let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);

        num = num / 10.0 + 0.5; // Translate to 0 -> 1
        if (num > 1 || num < 0) num = randn_bm(min, max, skew); // resample between 0 and 1 if out of range
        num = Math.pow(num, skew); // Skew
        num *= max - min; // Stretch to fill range
        num += min; // offset to min
        return num;
    }
};

module.exports = RequestGenerator;