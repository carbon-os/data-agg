// 26 options
// 12 Std : 14 Wvnt
// 10 en-US, 8 en-AU, 8 en-GB
// 13 MALE, 13 FEMALE


// duplicating some of these to boost the probability that they get chosen
// keeping male/female split, just upping the number of Wavenets vs Standards
const Voices = [
    // BEGIN Wavenets
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Wavenet-A",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Wavenet-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Wavenet-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Wavenet-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Wavenet-A",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Wavenet-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Wavenet-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Wavenet-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-A",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-E",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-F",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    // END Wavenets

    // BEGIN Duped Wavenets
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Wavenet-A",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Wavenet-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Wavenet-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Wavenet-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Wavenet-A",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Wavenet-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Wavenet-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Wavenet-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-A",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-E",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Wavenet-F",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    // END Duped Wavenets

    // BEGIN Standards
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Standard-A",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Standard-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Standard-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-GB"
        ],
        "name": "en-GB-Standard-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Standard-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Standard-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Standard-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-US"
        ],
        "name": "en-US-Standard-E",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Standard-A",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Standard-B",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Standard-C",
        "ssmlGender": "FEMALE",
        "naturalSampleRateHertz": 24000
    },
    {
        "languageCodes": [
            "en-AU"
        ],
        "name": "en-AU-Standard-D",
        "ssmlGender": "MALE",
        "naturalSampleRateHertz": 24000
    }
    // END Standards
];

module.exports = Voices;