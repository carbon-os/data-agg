const textToSpeech = require('@google-cloud/text-to-speech');
const ApiClient = require('./src/client');
const RequestGenerator = require('./src/request_generator');

const client = new textToSpeech.TextToSpeechClient();

const DATA_DIR = '/storage/robots_humans';

// (1) Test request
// const request = RequestGenerator.defaultRequest();
// ApiClient.callGoogleApiAndWriteToFile(client, request, DATA_DIR);


// (2) Loop random requests (Careful of rate limit!)
const SAMPLE_COUNT = 4;
for (let i = 0; i < SAMPLE_COUNT; i++) {
    const request = RequestGenerator.generateRequest();
    ApiClient.callGoogleApiAndWriteToFile(client, request, DATA_DIR);
}

