const fs = require('fs');
const limit = require("simple-rate-limiter");
const request = limit(require("request")).to(5).per(1000);

const Zips = require('./src/zips');
const Destinations = require('./src/destinations');

let n = 0;
const batchSize = 20;
const max = 460;

while (n < max) {

    const origins = Object.keys(Zips).slice(n, n + batchSize);
    const destinations = Destinations;

    const departure_time = "1536753600"; // Wednesday, September 12, 2018 8:00:00 AM GMT-04:00
    const mode = 'bicycling'; // transit, walking, driving, bicycling
    const key = 'AIzaSyBsob7qNgn25HmWE-jGGDj6wz4V0zHItUI';

    const url = 'https://maps.googleapis.com/maps/api/distancematrix/json?' +
        'origins=' + formatLocations(origins) +
        '&destinations=' + formatLocations(destinations) +
        '&mode=' + mode +
        '&departure_time=' + departure_time +
        '&key=' + key;
    // const url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=AIzaSyBsob7qNgn25HmWE-jGGDj6wz4V0zHItUI';

    console.log('calling url: ' + url);
    request(url, function (error, response, body) {


        const bodyJson = JSON.parse(body);

        if (bodyJson.status !== 'OK') {
            console.log("REQUEST ERROR: " + bodyJson.status);
            return;
        }

        const lines = bodyJson.rows.map((row, index) => {
            switch (mode) {
                case 'driving':
                    return printDrivingRow(origins, index, row);
                case 'transit':
                    return printTransitRow(origins, index, row);
                case 'bicycling':
                    return printBicyclingRow(origins, index, row);
                default:
                    throw Error('unknown mode: ' + mode);
            }
        });

        const csvLine = lines.reduce((a, b) => a + b);
        console.log("REQUEST ERROR: " + csvLine);

        fs.appendFile(mode + ".csv", csvLine, (err) => {
            if (err) {
                return console.log("Error writing to file: " + err);
            }
        });
    });

    n += batchSize;
}

function formatLocations(locations) {
    let output = '';
    let discriminator = '';
    for (let l of locations) {
        output = output + discriminator;
        output = output + l;
        discriminator = '|';
    }
    return output;
}

function printDrivingRow(origins, index, row) {
    return origins[index] + ', ' + row.elements.map(e => {
        if (e.status === 'OK') {
            return Math.round(e.distance.value / 1000) + ', ' + Math.round(e.duration.value / 60) + ', ' + Math.round(e.duration_in_traffic.value / 60) + ', ';
        } else {
            return ',';
        }
    }).reduce((a, b) => a + b) + '\n';
}

function printTransitRow(origins, index, row) {
    return origins[index] + ', ' + row.elements.map(e => {
        if (e.status === 'OK') {
            return Math.round(e.duration.value / 60) + ', ';
        } else {
            return ',';
        }
    }).reduce((a, b) => a + b) + '\n';
}

function printBicyclingRow(origins, index, row) {
    return origins[index] + ', ' + row.elements.map(e => {
        if (e.status === 'OK') {
            return Math.round(e.duration.value / 60) + ', ';
        } else {
            return ',';
        }
    }).reduce((a, b) => a + b) + '\n';
}