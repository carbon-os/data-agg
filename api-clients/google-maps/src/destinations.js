const Destinations = [
    "02138", // harvard sq
    "02116", // back bay
    "02110", // south station
    "01760", // natick
    "01602", // worcester
];

module.exports = Destinations;
