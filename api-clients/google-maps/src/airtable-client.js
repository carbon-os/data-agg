var Airtable = require('airtable');
var base = new Airtable({apiKey: 'keyysBKIoRhV8GEZ3'}).base('app5GIg5lIIb0yyHF');

const fields = [
    "Drive - Harvard Sq. (s)",
    "Drive w/Traffic - Harvard Sq. (s)",
    "Transit - Harvard Sq. (s)",
    "Bike - Harvard Sq. (s)",
    "Drive - Copley Sq. (s)",
    "Drive w/Traffic - Copley Sq. (s)",
    "Transit - Copley Sq. (s)",
    "Bike - Copley Sq. (s)",
    "Drive - Natick (s)",
    "Drive - Worcester (s)",
];

base('Zips').select({
    // Selecting the first 3 records in Main View:
    maxRecords: 500,
    view: "Main View"
}).eachPage(function page(records, fetchNextPage) {
    // This function (`page`) will get called for each page of records.

    records.forEach(function (record) {
        console.log('{ ', record.get('Name') + ':' + record.id + ' },');
    });

    // To fetch the next page of records, call `fetchNextPage`.
    // If there are more records, `page` will get called again.
    // If there are no more records, `done` will get called.
    fetchNextPage();

}, function done(err) {
    if (err) {
        console.error(err);
        return;
    }
});


// base('Zips').update('recOBozT1sZH2D1G9', {
//     "Drive - Harvard Sq. (s)": 2423
// }, function(err, record) {
//     if (err) { console.error(err); return; }
//     console.log(record.get('Drive - Harvard Sq. (s)'));
// });

