const Modes = [
    "transit",
    "bicycling",
    "walking",
    "driving"
];

module.exports = Modes;
