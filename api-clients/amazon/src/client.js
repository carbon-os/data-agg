const fs = require('fs');
const Voices = require('./voices');

const ApiClient = {

    callApiAndWriteToFile: (client, request, dir) => {

        client.synthesizeSpeech(request, (err, response) => {
            if (err) {
                console.error('ERROR:', err);
                throw Error("Error while calling Speech API: " + err)
            }

            const file = ApiClient.getFilename(request, dir);

            // Write the binary audio content to a local file
            fs.writeFile(file, response.AudioStream, 'binary', err => {
                if (err) {
                    console.error('ERROR:', err);
                    throw Error("Error while saving audiostream: " + err)
                }
                console.log(file);
            });

            // Append the result to the index file
            const indexRow = ApiClient.requestToString(request, file);
            const indexFile = dir + '/robots_amazon.csv';
            fs.appendFile(indexFile, indexRow, err => {
                if (err) {
                    console.error('ERROR:', err);
                }
            });
        });
    },

    getFilename: (request, dir) => {
        const timestamp = Date.now();
        return dir + '/aws/robot_' + ApiClient.getGender(request).toLowerCase() + '_aws-' + request.VoiceId + '_' + timestamp + '.mp3';
    },

    getGender: (request) => {
        for (let voice of Voices) {
            if (voice.Id === request.VoiceId) {
                return voice.Gender;
            }
        }
    },

    requestToString: (request, filename) => {
        return "\"" + filename + "\", \""
            + request.VoiceId + "\", \""
            + ApiClient.getGender(request).toLowerCase() + "\", \""
            + request.Text.replace("\"", "\"\"") + "\",\n";
    }

};

module.exports = ApiClient;