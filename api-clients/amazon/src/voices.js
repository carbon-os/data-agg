var Voices = [
    {
        "Gender": "Male",
        "Id": "Geraint",
        "LanguageCode": "en-GB-WLS",
        "LanguageName": "Welsh English",
        "Name": "Geraint"
    },
    {
        "Gender": "Female",
        "Id": "Salli",
        "LanguageCode": "en-US",
        "LanguageName": "US English",
        "Name": "Salli"
    },
    {
        "Gender": "Male",
        "Id": "Matthew",
        "LanguageCode": "en-US",
        "LanguageName": "US English",
        "Name": "Matthew"
    },
    {
        "Gender": "Female",
        "Id": "Kimberly",
        "LanguageCode": "en-US",
        "LanguageName": "US English",
        "Name": "Kimberly"
    },
    {
        "Gender": "Female",
        "Id": "Kendra",
        "LanguageCode": "en-US",
        "LanguageName": "US English",
        "Name": "Kendra"
    },
    {
        "Gender": "Male",
        "Id": "Justin",
        "LanguageCode": "en-US",
        "LanguageName": "US English",
        "Name": "Justin"
    },
    {
        "Gender": "Male",
        "Id": "Joey",
        "LanguageCode": "en-US",
        "LanguageName": "US English",
        "Name": "Joey"
    },
    {
        "Gender": "Female",
        "Id": "Joanna",
        "LanguageCode": "en-US",
        "LanguageName": "US English",
        "Name": "Joanna"
    },
    {
        "Gender": "Female",
        "Id": "Ivy",
        "LanguageCode": "en-US",
        "LanguageName": "US English",
        "Name": "Ivy"
    },
    {
        "Gender": "Female",
        "Id": "Raveena",
        "LanguageCode": "en-IN",
        "LanguageName": "Indian English",
        "Name": "Raveena"
    },
    {
        "Gender": "Female",
        "Id": "Aditi",
        "LanguageCode": "en-IN",
        "LanguageName": "Indian English",
        "Name": "Aditi"
    },
    {
        "Gender": "Female",
        "Id": "Emma",
        "LanguageCode": "en-GB",
        "LanguageName": "British English",
        "Name": "Emma"
    },
    {
        "Gender": "Male",
        "Id": "Brian",
        "LanguageCode": "en-GB",
        "LanguageName": "British English",
        "Name": "Brian"
    },
    {
        "Gender": "Female",
        "Id": "Amy",
        "LanguageCode": "en-GB",
        "LanguageName": "British English",
        "Name": "Amy"
    },
    {
        "Gender": "Male",
        "Id": "Russell",
        "LanguageCode": "en-AU",
        "LanguageName": "Australian English",
        "Name": "Russell"
    },
    {
        "Gender": "Female",
        "Id": "Nicole",
        "LanguageCode": "en-AU",
        "LanguageName": "Australian English",
        "Name": "Nicole"
    },
];

module.exports = Voices;