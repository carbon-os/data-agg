const Voices = require('./voices');
const Sentences = require('./sentences');

const RequestGenerator = {

    AUDIO_ENCODING: 'mp3',

    SAMPLE_RATE: '16000',

    TEXT_TYPE: 'text',

    generateRequest: () => {
        return {
            OutputFormat: RequestGenerator.AUDIO_ENCODING,
            SampleRate: RequestGenerator.SAMPLE_RATE,
            Text: RequestGenerator.generateRandomText(),
            TextType: RequestGenerator.TEXT_TYPE,
            VoiceId: RequestGenerator.generateRandomVoice(),
        };
    },

    defaultRequest: () => {
        return {
            OutputFormat: "mp3",
            SampleRate: "16000",
            Text: "All Gaul is divided into three parts",
            TextType: "text",
            VoiceId: "Joanna",
        };
    },

    generateRandomVoice: () => {
        const index = RequestGenerator.randomInt(0, Voices.length);
        return Voices[index].Id;
    },

    generateRandomText: () => {
        const index = RequestGenerator.randomInt(0, Sentences.length);
        return Sentences[index];
    },

    randomInt: (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; // The maximum is exclusive and the minimum is inclusive
    },

    // it will make it to ~half of the min max some low percentage of the time ~2%
    // it will pick the mean ~40% of the time
    randomIntNormalDistribution: (min, max, skew) => {
        let u = 0, v = 0;
        while (u === 0) u = Math.random(); //Converting [0,1) to (0,1)
        while (v === 0) v = Math.random();
        let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);

        num = num / 10.0 + 0.5; // Translate to 0 -> 1
        if (num > 1 || num < 0) num = randn_bm(min, max, skew); // resample between 0 and 1 if out of range
        num = Math.pow(num, skew); // Skew
        num *= max - min; // Stretch to fill range
        num += min; // offset to min
        return num;
    }
};

module.exports = RequestGenerator;