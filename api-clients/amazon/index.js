const AWS = require('aws-sdk');
const ApiClient = require('./src/client');
const RequestGenerator = require('./src/request_generator');

AWS.config.update({region:'us-east-1'});
const client = new AWS.Polly({apiVersion: '2016-06-10'});

const DATA_DIR = '/storage/robots_humans';

// (1) Test request
// const request = RequestGenerator.defaultRequest();
// ApiClient.callApiAndWriteToFile(client, request, DATA_DIR);


// (2) Loop random requests (Careful of rate limit!)
const SAMPLE_COUNT = 2800;
for (let i = 0; i < SAMPLE_COUNT; i++) {
    const request = RequestGenerator.generateRequest();
    ApiClient.callApiAndWriteToFile(client, request, DATA_DIR);
}


