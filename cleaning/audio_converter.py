import logging
import os
import audiotools
import librosa
import scipy.io.wavfile as sci_wav

from audiotools.flac import InvalidFLAC
from pydub import AudioSegment

"""
Flac to Wav converter script
using audiotools
"""

DEFAULT_SAMPLE_RATE = 16000

def convertFlacsToWavs(in_path, out_path, out_file_prefix=""):
    files_to_process = []

    # collect files to be converted
    for root, dirs, files in os.walk(in_path):
        for file in files:
            if file.endswith(".flac"):
                file_flac = os.path.join(root, file)
                print("Found file for queue: " + file_flac)
                files_to_process.append(file_flac)

    print("Start converting: %s files", str(len(files_to_process)))

    for file_flac in files_to_process:
        compression_quality = '0'  # min compression
        file_wav = (out_path + "/" + out_file_prefix + file_flac.split("/")[-1]).replace(".flac", ".wav")

        try:
            audiotools.open(file_flac).convert(file_wav, audiotools.WaveAudio, compression_quality)

            print(''.join(["Converted ", file_flac, " to: ", file_wav]))
        except InvalidFLAC:
            print(''.join(["Failed to open file ", file_flac, " to: ", file_wav, " failed."]), exc_info=True)
        except Exception as e:
            print('ExFailed to open file', exc_info=True)

    print("Completed conversion")


def convertMp3sToWavs(in_path, out_path, out_file_prefix=""):
    files_to_process = []

    # collect files to be converted
    for root, dirs, files in os.walk(in_path):
        for file in files:
            if file.endswith(".mp3"):
                file_mp3 = os.path.join(root, file)
                print("Found file for queue: " + file_mp3)
                files_to_process.append(file_mp3)

    print("Start converting: %s files", str(len(files_to_process)))

    for file_mp3 in files_to_process:
        compression_quality = '0'  # min compression
        file_wav = (out_path + "/" + out_file_prefix + file_mp3.split("/")[-1]).replace(".mp3", ".wav")

        recording = AudioSegment.from_mp3(file_mp3)
        recording.export(file_wav, format="wav")

        ## print('new file samplerate: ' + str(sci_wav.read(file_wav)[0]))
        print(''.join(["Converted ", file_mp3, " to: ", file_wav]))


    print("Completed")


def downsample(path):
    files_to_process = []

    # collect files to be converted
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(".wav"):
                file_wav = os.path.join(root, file)
                print("Found file for queue: " + file_wav)
                files_to_process.append(file_wav)

    print("Start converting: %s files", str(len(files_to_process)))

    for file_wav in files_to_process:
        print('before: ' + str(sci_wav.read(file_wav)[0]))
        y, sr = librosa.load(file_wav, sr=DEFAULT_SAMPLE_RATE)
        librosa.output.write_wav(file_wav, y, sr)
        print('after: ' + str(sci_wav.read(file_wav)[0]))

    print("Completed downsampling")